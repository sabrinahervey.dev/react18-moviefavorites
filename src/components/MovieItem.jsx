import PropTypes from "prop-types";

import "./MovieItem.css";
import { useState } from "react";

function MovieItem(props) {
  const { title, released, director, poster } = props;
  
  //on défénit le composant movieItem
  const [isFavorite, setFavorite] = useState(false);
  //fonction quand tu clique sur le boutton tu l'ajoute au favori
  function toggleFavorite() {
    //si le film est déjà favori, on le retire avec false
    if (isFavorite) {
      setFavorite(false);
    } else {
    //si le film n'est pas favori, on l'ajoute avec true
      setFavorite(true);
    }
  };

  return (
    <div className="MovieItem">
      <h2>{title}</h2>
      <img src={poster} alt={title} />
      <h4>Director: {director}</h4>
      <h5>Released: {released}</h5>  
      <button type="button" onClick={toggleFavorite}>
        {isFavorite ? "film favori" : "Ajouter ce film aux favoris"}
      </button>
      
    </div>
  );
}

MovieItem.propTypes = {
  title: PropTypes.string.isRequired,
  released: PropTypes.string.isRequired,
  director: PropTypes.string.isRequired,
  poster: PropTypes.string.isRequired,
};

export default MovieItem;
